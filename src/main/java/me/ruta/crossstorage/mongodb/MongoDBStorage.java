package me.ruta.crossstorage.mongodb;

import me.ruta.crossstorage.api.Storage;
import me.ruta.crossstorage.api.StorageType;
import me.ruta.crossstorage.api.Connector;
import org.bukkit.configuration.ConfigurationSection;

public class MongoDBStorage extends Storage {
    public MongoDBStorage(ConfigurationSection where) {
        super(StorageType.MONGODB, where);
    }

    @Override
    protected boolean setup() {
        // TODO: download library
        return true;
    }

    @Override
    protected Connector getConnector() {
        return new MongoDBConnector();
    }
}
