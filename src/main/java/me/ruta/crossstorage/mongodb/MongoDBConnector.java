package me.ruta.crossstorage.mongodb;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import me.ruta.crossstorage.api.Connector;
import me.ruta.crossstorage.api.Platform;

public class MongoDBConnector implements Connector {
    @Override
    public Platform connect(String host, int port, String database) {
        MongoClient connection = MongoClients.create("mongodb://" + host + ":" + port);
        return new MongoDBPlatform(connection, connection.getDatabase(database), database);
    }

    @Override
    public Platform connect(String host, int port, String database, String user, String password) {
        MongoClient connection = MongoClients.create(
                "mongodb://" + user + ":" + password + "@" + host + ":" + port + "/?authSource=" + database + "&authMechanism=SCRAM-SHA-1");
        return new MongoDBPlatform(connection, connection.getDatabase(database), database);
    }
}
