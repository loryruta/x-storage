package me.ruta.crossstorage.mongodb;

import com.mongodb.client.MongoCollection;
import lombok.Getter;
import me.ruta.crossstorage.api.Element;
import me.ruta.crossstorage.api.Table;
import org.bson.Document;

public class MongoDBTable implements Table {
    private MongoCollection<Document> collection;

    @Getter
    private String name;

    public MongoDBTable(MongoCollection<Document> collection, String name) {
        this.collection = collection;
        this.name = name;
    }

    @Override
    public boolean create() {
        return true;
    }

    @Override
    public void clear() {
        collection.deleteMany(new Document());
    }

    @Override
    public boolean drop() {
        collection.drop();
        return true;
    }

    @Override
    public Element getElement(String id) {
        return new MongoDBElement(collection, id);
    }
}
