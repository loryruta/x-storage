package me.ruta.crossstorage.mongodb;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import me.ruta.crossstorage.api.Platform;
import me.ruta.crossstorage.api.Table;

public class MongoDBPlatform implements Platform {
    private MongoClient connection;
    private MongoDatabase database;

    @Getter
    private String name;

    public MongoDBPlatform(MongoClient connection, MongoDatabase database, String name) {
        this.connection = connection;
        this.database = database;
        this.name = name;
    }

    @Override
    public void disconnect() {
        connection.close();
    }

    @Override
    public boolean drop() {
        database.drop();
        return true;
    }

    @Override
    public Table getTable(String name) {
        return new MongoDBTable(database.getCollection(name), name);
    }
}
