package me.ruta.crossstorage.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import me.ruta.crossstorage.api.Element;
import org.bson.Document;

import java.util.Map;

public class MongoDBElement implements Element {
    private MongoCollection<Document> collection;

    private String id;

    public MongoDBElement(MongoCollection<Document> collection, String id) {
        this.collection = collection;
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Object get(String parameter) {
        Document projection = new Document();
        String[] path = parameter.split("\\.");
        int i;
        Document tmp = null;
        for (i = 0; i < path.length - 1; i++) {
            tmp = new Document();
            projection.put(path[i], tmp);
        }
        if (tmp != null) {
            tmp.put(path[i], 1);
        }
        return collection.find(new Document("_id", id)).projection(projection).first();
    }

    @Override
    public boolean put(Map<String, Object> data) {
        UpdateResult result = collection.updateOne(new Document("_id", id), new Document(data), new UpdateOptions().upsert(true));
        return result.getModifiedCount() > 0;
    }

    @Override
    public boolean drop() {
        DeleteResult result = collection.deleteOne(new Document("_id", id));
        return result.getDeletedCount() > 0;
    }
}
