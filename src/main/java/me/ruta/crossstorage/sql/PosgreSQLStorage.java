package me.ruta.crossstorage.sql;

import me.ruta.crossstorage.api.Storage;
import me.ruta.crossstorage.api.StorageType;
import org.bukkit.configuration.ConfigurationSection;

public class PosgreSQLStorage extends Storage {
    public PosgreSQLStorage(ConfigurationSection where) {
        super(StorageType.POSGRESQL, where);
    }

    @Override
    protected boolean setup() {
        return true;
    }

    @Override
    protected SQLConnector getConnector() {
        return new ConnectorImpl();
    }

    public static class ConnectorImpl extends SQLConnector {
        @Override
        public String getUrl(String host, int port) {
            return "jdbc:posgresql://" + host + ":" + port;
        }
    }
}
