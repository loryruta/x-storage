package me.ruta.crossstorage.sql;

import me.ruta.crossstorage.api.Element;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * On SQL a document is composed of three column: id, parameter and value.
 */
public class SQLElement implements Element {
    private final Connection connection;
    private final SQLTable table;
    private final String id;

    public SQLElement(Connection connection, SQLTable table, String id) {
        this.connection = connection;
        this.table = table;
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Object get(String parameter) {
        String query = "SELECT value FROM " + table.getName() + " WHERE id=? AND parameter=?";
        ResultSet result;
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, id);
            statement.setString(2, parameter);

            result = statement.executeQuery();
            result.first();

            return result.getObject("value");
        } catch (SQLException exception) {
            throw new IllegalStateException(exception);
        }
    }

    @SuppressWarnings("unchecked")
    private int put(String path, Map<String, Object> current, int changes) {
        for (Map.Entry<String, Object> entry : current.entrySet()) {
            if (entry.getValue() instanceof Map) {
                put(path + "." + entry.getKey() + ".", (Map<String, Object>) entry.getValue(), changes);
                continue;
            }
            String query = "INSERT INTO " + table.getName() + " (id, parameter, value) VALUES (?, ?, ?)";
            try {
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, id);
                statement.setString(2, path + entry.getKey());
                statement.setString(3, entry.getValue().toString());
                changes += statement.executeUpdate();

            } catch (SQLException exception) {
                throw new IllegalStateException(exception);
            }
        }
        return changes;
    }

    @Override
    public boolean put(Map<String, Object> data) {
        return put("", data, 0) > 0;
    }

    @Override
    public boolean drop() {
        String query = "DELETE FROM " + table.getName() + " WHERE id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, id);
            return statement.executeUpdate() > 0;

        } catch (SQLException exception) {
            throw new IllegalStateException(exception);
        }
    }
}
