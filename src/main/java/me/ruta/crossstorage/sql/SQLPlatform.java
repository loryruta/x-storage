package me.ruta.crossstorage.sql;

import lombok.Getter;
import me.ruta.crossstorage.api.Platform;
import me.ruta.crossstorage.api.Table;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQLPlatform implements Platform {
    private Connection connection;

    @Getter
    private String name;

    public SQLPlatform(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException exception) {
            throw new IllegalStateException(exception);
        }
    }

    @Override
    public boolean drop() {
        String query = "DROP SCHEMA IF EXISTS " + name;
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            return statement.executeUpdate() > 0;

        } catch (SQLException exception) {
            throw new IllegalStateException(exception);
        }
    }

    @Override
    public Table getTable(String name) {
        return new SQLTable(connection, this, name);
    }
}
