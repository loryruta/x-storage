package me.ruta.crossstorage.sql;

import me.ruta.crossstorage.api.Element;
import me.ruta.crossstorage.api.Table;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQLTable implements Table {
    private SQLPlatform platform;
    private Connection connection;
    private String name;

    public SQLTable(Connection connection, SQLPlatform platform, String name) {
        this.connection = connection;
        this.platform = platform;
        this.name = name;
    }

    @Override
    public String getName() {
        return platform.getName() + "." + name;
    }

    @Override
    public boolean create() {
        String query = "CREATE TABLE IF NOT EXISTS " + getName() + " (" +
                "id VARCHAR(256)," +
                "parameter VARCHAR(512)," +
                "value VARCHAR(512)," +
                "CONSTRAINT PK_Parameter PRIMARY KEY (id, parameter)" +
                ")";
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            return statement.executeUpdate() > 0;

        } catch (SQLException exception) {
            throw new IllegalStateException(exception);
        }
    }

    @Override
    public void clear() {
        String query = "DELETE FROM " + getName();
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();

        } catch (SQLException exception) {
            throw new IllegalStateException(exception);
        }
    }

    @Override
    public boolean drop() {
        String query = "DROP TABLE IF EXISTS " + getName();
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            return statement.executeUpdate() > 0;

        } catch (SQLException exception) {
            throw new IllegalStateException(exception);
        }
    }

    @Override
    public Element getElement(String id) {
        return new SQLElement(connection, this, id);
    }
}
