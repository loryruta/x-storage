package me.ruta.crossstorage.sql;

import me.ruta.crossstorage.api.Connector;
import me.ruta.crossstorage.api.Platform;

import java.sql.Connection;
import java.sql.DriverManager;

public abstract class SQLConnector implements Connector {
    public abstract String getUrl(String host, int port);

    @Override
    public Platform connect(String host, int port, String database) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection(getUrl(host, port));
            return new SQLPlatform(connection);

        } catch (Exception exception) {
            throw new IllegalStateException(exception);
        }
    }

    @Override
    public Platform connect(String host, int port, String database, String user, String password) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection(getUrl(host, port), user, password);
            return new SQLPlatform(connection);

        } catch (Exception exception) {
            throw new IllegalStateException(exception);
        }
    }
}
