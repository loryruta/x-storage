package me.ruta.crossstorage.sql;

import me.ruta.crossstorage.api.Storage;
import me.ruta.crossstorage.api.StorageType;
import org.bukkit.configuration.ConfigurationSection;

public class MySQLStorage extends Storage {
    public MySQLStorage(ConfigurationSection where) {
        super(StorageType.MYSQL, where);
    }

    @Override
    protected boolean setup() {
        return true;
    }

    @Override
    protected SQLConnector getConnector() {
        return new ConnectorImpl();
    }

    public static class ConnectorImpl extends SQLConnector {
        @Override
        public String getUrl(String host, int port) {
            return "jdbc:mysql://" + host + ":" + port;
        }
    }
}
