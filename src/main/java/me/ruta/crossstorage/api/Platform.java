package me.ruta.crossstorage.api;

public interface Platform {
    String getName();

    void disconnect();

    boolean drop();

    Table getTable(String name);
}
