package me.ruta.crossstorage.api;

public interface Table {
    String getName();

    boolean create();

    void clear();

    boolean drop();

    Element getElement(String id);
}
