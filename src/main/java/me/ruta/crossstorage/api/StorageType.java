package me.ruta.crossstorage.api;

import lombok.Getter;
import me.ruta.crossstorage.mongodb.MongoDBStorage;
import me.ruta.crossstorage.sql.MariaDbStorage;
import me.ruta.crossstorage.sql.MySQLStorage;
import me.ruta.crossstorage.sql.PosgreSQLStorage;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashMap;
import java.util.Map;

public enum StorageType {
    MYSQL("mysql") {
        @Override
        public Storage create(ConfigurationSection configuration) {
            return new MySQLStorage(configuration);
        }
    },
    MARIADB("mariadb") {
        @Override
        public Storage create(ConfigurationSection configuration) {
            return new MariaDbStorage(configuration);
        }
    },
    POSGRESQL("posgresql") {
        @Override
        public Storage create(ConfigurationSection configuration) {
            return new PosgreSQLStorage(configuration);
        }
    },
    MONGODB("mongodb") {
        @Override
        public Storage create(ConfigurationSection configuration) {
            return new MongoDBStorage(configuration);
        }
    };

    private static final Map<String, StorageType> BY_NAME = new HashMap<>();

    static {
        for (StorageType value : values()) {
            BY_NAME.put(value.name, value);
        }
    }

    @Getter
    private String name;

    StorageType(String name) {
        this.name = name;
    }

    public abstract Storage create(ConfigurationSection configuration);

    public static StorageType getByName(String name) {
        return BY_NAME.get(name);
    }
}
