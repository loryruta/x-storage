package me.ruta.crossstorage.api;

import java.util.Map;

public interface Element {
    String getId();

    Object get(String parameter);

    boolean put(Map<String, Object> data);

    boolean drop();
}
