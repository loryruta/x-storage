package me.ruta.crossstorage.api;

import lombok.Getter;
import org.bukkit.configuration.ConfigurationSection;

public abstract class Storage {
    @Getter
    private StorageType type;

    private final String host;
    private final int port;
    private final String database;

    private final boolean auth;
    private String user;
    private String password;

    public Storage(StorageType type, ConfigurationSection where) {
        this.type = type;

        this.host = where.getString("host");
        this.port = where.getInt("port");
        this.database = where.getString("target");

        if (this.auth = (where.contains("user") || where.contains("password"))) {
            this.user = where.getString("user");
            this.password = where.getString("password");
        }
    }

    protected abstract boolean setup();

    protected abstract Connector getConnector();

    protected Platform connect() {
        Connector connector = getConnector();
        if (auth) {
            return connector.connect(host, port, database, user, password);
        } else {
            return connector.connect(host, port, database);
        }
    }

    public Platform handshake() {
        if (setup()) {
            return connect();
        }
        throw new IllegalStateException();
    }
}
