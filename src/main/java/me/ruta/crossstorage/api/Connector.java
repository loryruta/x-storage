package me.ruta.crossstorage.api;

public interface Connector {
    Platform connect(String host, int port, String database);

    Platform connect(String host, int port, String database, String user, String password);
}
