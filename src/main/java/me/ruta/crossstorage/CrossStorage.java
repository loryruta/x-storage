package me.ruta.crossstorage;

import me.ruta.crossstorage.api.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class CrossStorage extends JavaPlugin {
    @Override
    public void onEnable() {
        getLogger().info("Enabling");
    }

    @Override
    public void onDisable() {
        getLogger().info("Disabling");
    }

    /**
     * Retrieves an instance of the Storage selected by the given plugin ready to open a connection.
     * The function will search for the 'select_database.yml' configuration file, in the plugin data folder.
     *
     * @param plugin the dependant plugin
     * @return the storage
     */
    public static Storage retrieve(Plugin plugin) {
        File file = new File(plugin.getDataFolder(), "select_database.yml");
        if (!file.exists()) {
            throw new IllegalArgumentException("File not found: select_database.yml");
        }
        ConfigurationSection config = YamlConfiguration.loadConfiguration(file).getConfigurationSection("storage");
        String platform = config.getString("platform_name");
        StorageType result = StorageType.getByName(platform);
        if (result == null) {
            throw new IllegalArgumentException("Platform not supported: " + platform);
        }
        return result.create(config.getConfigurationSection("auth"));
    }
}
